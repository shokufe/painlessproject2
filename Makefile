SRCS = $(shell find . -name "*.cpp") 

OBJS = $(addsuffix .o, $(basename $(SRCS)))

EXEC = painless

LIBS =  -lmapleCOMSPS -L./tools/mapleCOMSPS/build/release/lib/ \
		-lminisat -L./tools/minisat/build/release/lib/ \
		-lglucose -L./tools/glucose/parallel \
       -lpthread -lz -lm
       
 #-lglucose -L../../tools/glucose/parallel 
       #-lminisat -L../minisat/build/release/lib/ 
       #-llgl -L../lingeling/ 
    
	


CXXFLAGS = -I./tools/mapleCOMSPS \
           -D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS -std=c++11 -O3

		#-I../glucose -I../minisat -I../lingeling -I../mapleCOMSPS \
         #-D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS -std=c++11 -O3
		
release: CXXFLAGS += -D NDEBUG
release: $(EXEC)

debug: CXXFLAGS += -DEBUG -g
debug: $(EXEC)

$(EXEC): $(OBJS)
	g++ ./features/.output.test/*.cpp -o me.exe -Wall -fpermissive $(CXXFLAGS) $(LIBS)
	#$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBS)
	
	
	

%.o: %.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS) $(LIBS)

clean:
	rm -f $(OBJS) $(EXEC)
