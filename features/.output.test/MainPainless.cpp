#include "MainPainless.h"

#line 29 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"


int main(int argc, char ** argv) {

	//create instance of "test"
	MainPainless myHello;

	//run Hello::run as entry-point of the app
	return myHello.run(argc,argv);
}
/*
#include "../../tools/painless-src/painless.h"

#include "../../tools/painless-src/clauses/ClauseManager.h"
#include "../../tools/painless-src/sharing/SimpleSharing.h"
#include "../../tools/painless-src/sharing/HordeSatSharing.h"
#include "../../tools/painless-src/sharing/Sharer.h"
#include "../../tools/painless-src/solvers/SolverFactory.h"
#include "../../tools/painless-src/utils/Logger.h"
#include "../../tools/painless-src/utils/Parameters.h"
#include "../../tools/painless-src/utils/SatUtils.h"
#include "../../tools/painless-src/utils/System.h"
#include "../../tools/painless-src/working/SequentialWorker.h"
#include "../../tools/painless-src/working/Portfolio.h"
#include "../../tools/painless-src/working/CubeAndConquer.h"

#include <unistd.h>

using namespace std;


// -------------------------------------------
// Declaration of global variables
// -------------------------------------------
atomic<bool> globalEnding(false);

Sharer ** sharers = NULL;

int nSharers = 0;

WorkingStrategy * working = NULL;

SatResult finalResult = UNKNOWN;

vector<int> finalModel;


// -------------------------------------------
// Main du framework
// -------------------------------------------
int main(int argc, char ** argv)
{
   Parameters::init(argc, argv);

   if (Parameters::getFilename() == NULL ||
       Parameters::isSet("h"))
   {
      printf("USAGE: %s [parameters] input.cnf\n", argv[0]);
      printf("Parameters:\n");

#line 89 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
      printf("\t-solver=type\t\t glucose, lingeling, minisat, maple, or"              " combo\n");
      printf("\t-lbd-limit=<INT>\t lbd limit of exported clauses\n");

#line 93 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
      printf("\t-d=0...7\t\t diversification 0=none, 1=sparse, 2=dense,"              " 3=random, 4=native, 5=1&4, 6=sparse-random, 7=6&4,"              " default is 0.\n");
      printf("\t-c=<INT>\t\t number of cpus, default is 4.\n");

#line 96 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
      printf("\t-wkr-strat=1...2\t 1=portfolio, 2=cube and conquer,"              " default is portfolio\n");

#line 98 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
      printf("\t-shr-strat=1...2\t 1=alltoall, 2=hordesat sharing,"              " default is 1\n");

#line 100 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
      printf("\t-shr-sleep=<INT>\t time in usecond a sharer sleep each"              " round, default 500000 (0.5s)\n");

#line 102 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
      printf("\t-shr-lit=<INT>\t\t number of literals shared per round by the"              " hordesat strategy, default is 1500\n");
      printf("\t-no-model\t\t won't print the model if the problem is SAT\n");
      printf("\t-t=<INT>\t\t timeout in second, default is no limit\n");

      return 0;
   }

   Parameters::printParams();

   setVerbosityLevel(Parameters::getIntParam("v", 0));

   int cpus = Parameters::getIntParam("c", 4);

   srand(time(NULL));

   // Create solvers
   vector<SolverInterface *> solvers;

   const string solverType = Parameters::getParam("solver");

   int nSolvers = cpus;
   if (Parameters::getIntParam("wkr-strat", 1) == 2) {
       nSolvers = 1;
   } else if (Parameters::getIntParam("wkr-strat", 1) == 3) {
      nSolvers /= 3;
   }

   if (solverType == "glucose") {
      SolverFactory::createGlucoseSolvers(nSolvers, solvers);
   } else if (solverType == "lingeling") {
      SolverFactory::createLingelingSolvers(nSolvers, solvers);
   } else if (solverType == "maple") {
      SolverFactory::createMapleSolvers(nSolvers, solvers);
   } else if (solverType == "combo") {
      SolverFactory::createComboSolvers(nSolvers, solvers);
   } else {
      // MiniSat is the default choice
      SolverFactory::createMiniSatSolvers(nSolvers, solvers);
   }

   // Diversifycation
   int diversification = Parameters::getIntParam("d", 0);

   switch (diversification) {
      case 1 :
         SolverFactory::sparseDiversification(solvers);
         break;

      case 2 :
         SolverFactory::binValueDiversification(solvers);
         break;

      case 3 :
         SolverFactory::randomDiversification(solvers, 2015);
         break;

      case 4 :
         SolverFactory::nativeDiversification(solvers);
         break;

      case 5 :
         SolverFactory::sparseDiversification(solvers);
         SolverFactory::nativeDiversification(solvers);
         break;

      case 6 :
         SolverFactory::sparseRandomDiversification(solvers);
         break;

      case 7 :
         SolverFactory::sparseRandomDiversification(solvers);
         SolverFactory::nativeDiversification(solvers);
         break;

      case 0 :
         break;
   }

   if (Parameters::getIntParam("wkr-strat", 1) == 3) {
      solvers.push_back(SolverFactory::createLingelingSolver());
   }

   vector<SolverInterface *> from;
   // Start sharing threads
   switch(Parameters::getIntParam("shr-strat", 0)) {
      case 1 :
         nSharers   = 1;
         sharers    = new Sharer*[nSharers];
         sharers[0] = new Sharer(0, new SimpleSharing(), solvers, solvers);
         break;
      case 2 :
         nSharers = cpus;
         sharers  = new Sharer*[nSharers];

         for (size_t i = 0; i < nSharers; i++) {
            from.clear();
            from.push_back(solvers[i]);
            sharers[i] = new Sharer(i, new HordeSatSharing(), from,
                                    solvers);
         }
         break;
      case 3 :
         nSharers = 1;
         sharers  = new Sharer*[nSharers];
         sharers[0] = new Sharer(0, new HordeSatSharing(), solvers, solvers);
         break;

      case 4:
         nSharers = 2;
         sharers  = new Sharer*[nSharers];

         for (size_t i=0; i < nSolvers; i++) {
            from.push_back(solvers[i]);
         }
         sharers[0] = new Sharer(0, new SimpleSharing(), from, solvers);

         from.clear();
         from.push_back(solvers[nSolvers]);
         sharers[1] = new Sharer(1, new HordeSatSharing(), from, solvers);
         break;

      case 0 :
         break;
   }

   WorkingStrategy * childPF, *childCC;
   // Working strategy creation
   switch(Parameters::getIntParam("wkr-strat", 1)) {
      case 1 :
         working = new Portfolio();
         for (size_t i = 0; i < cpus; i++) {
            working->addSlave(new SequentialWorker(solvers[i]));
         }
         break;

      case 2 :
         working = new CubeAndConquer(cpus);
         working->addSlave(new SequentialWorker(solvers[0]));
         break;

      case 3 :
         working = new Portfolio();

         childPF = new Portfolio();
         for (size_t i = 0; i < nSolvers; i++) {
            childPF->addSlave(new SequentialWorker(solvers[i]));
         }
         working->addSlave(childPF);

         childCC = new CubeAndConquer(cpus - nSolvers);
         childCC->addSlave(new SequentialWorker(solvers[nSolvers]));
         working->addSlave(childCC);
         break;

      case 0 :
         break;
   }

   // Init the management of clauses
   ClauseManager::initClauseManager();

   // Launch working
   vector<int> cube;
   working->solve(cube);

   // Wait until end or timeout
   int timeout   = Parameters::getIntParam("t", -1);
   int maxMemory = Parameters::getIntParam("max-memory", -1) * 1024 * 1024;

   while(globalEnding == false) {
      sleep(1);

      if (maxMemory > 0 && getMemoryUsed() > maxMemory) {
         cout << "c Memory used is going too large!!!!" << endl;
      }

      if (timeout > 0 && getRelativeTime() >= timeout) {
         globalEnding = true;
         working->setInterrupt();
      }
   }

   // Delete sharers
   for (int i=0; i < nSharers; i++) {
      delete sharers[i];
   }
   delete sharers;

   // Delete working strategy
   delete working;

   // Delete shared clauses
   ClauseManager::joinClauseManager();

   // Print solver stats
   SolverFactory::printStats(solvers);

   // Print the result and the model if SAT
   if (finalResult == SAT) {
      printf("s SATISFIABLE\n");

      if (Parameters::isSet("no-model") == false) {
         printModel(finalModel);
      }
   } else if (finalResult == UNSAT) {
      printf("s UNSATISFIABLE\n");
   } else {
      printf("s UNKNOWN\n");
   }

   return finalResult;
}
*/

#line 27 "/home/shokoufeh/TestPainless/features/working/MainPainless.h"





#line 12 "/home/shokoufeh/TestPainless/features/DivideAndConquer/MainPainless.h"


#line 12 "/home/shokoufeh/TestPainless/features/SequentialeEngine/MainPainless.h"


#line 13 "/home/shokoufeh/TestPainless/features/CDCL/MainPainless.h"


#line 12 "/home/shokoufeh/TestPainless/features/Maple/MainPainless.h"


