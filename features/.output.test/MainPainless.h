#ifndef MainPainless_h__included
#define MainPainless_h__included

//Layer Painless


#line 1 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
// -----------------------------------------------------------------------------
// Copyright (C) 2017  Ludovic LE FRIOUX
//
// This file is part of PaInleSS.
//
// PaInleSS is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

//#include "../../tools/painless-src/painless.h"
#include <unistd.h>
#include <stdio.h>

//Layer working


#line 1 "/home/shokoufeh/TestPainless/features/working/MainPainless.h"
/*#include "../../tools/painless.h"

#include "../../tools/clauses/ClauseManager.h"

#include "../../tools/sharing/HordeSatSharing.h"
#include "../Sharing/Sharer.h"
#include "../../tools/solvers/SolverFactory.h"
#include "../../tools/utils/Logger.h"
#include "../../tools/utils/Parameters.h"
#include "../../tools/utils/SatUtils.h"
#include "../../tools/utils/System.h"
#include "../../tools/working/SequentialWorker.h"
#include "../Portfolio/Portfolio.h"
//#include "../DivideAndConquer/CubeAndConquer.h"
#include "../Painless/MainPainless.h"


*/


//Layer DivideAndConquer


#line 1 "/home/shokoufeh/TestPainless/features/DivideAndConquer/MainPainless.h"
#include "../../tools/painless-src/working/CubeAndConquer.h"

//Layer SequentialeEngine


#line 1 "/home/shokoufeh/TestPainless/features/SequentialeEngine/MainPainless.h"


//Layer CDCL


#line 1 "/home/shokoufeh/TestPainless/features/CDCL/MainPainless.h"


//Layer Maple


#line 1 "/home/shokoufeh/TestPainless/features/Maple/MainPainless.h"
#include "../../tools/painless-src/solvers/Maple.h"


#line 23 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
class MainPainless {
//**** Layer Painless ****
private:
#line 23 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"

public:
	inline 
#line 25 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
int Painless_run(int argc, char **argv){
			printf("mainPainless\n" );
			return 0;
	}

//**** Layer working ****
private:
#line 20 "/home/shokoufeh/TestPainless/features/working/MainPainless.h"

public:
	inline 
#line 22 "/home/shokoufeh/TestPainless/features/working/MainPainless.h"
int working_run(int argc, char **argv) {
		int res = Painless_run(argc,argv);
		printf("working");
				return 0;
			}

//**** Layer DivideAndConquer ****
private:
#line 2 "/home/shokoufeh/TestPainless/features/DivideAndConquer/MainPainless.h"

public:
	inline 
#line 4 "/home/shokoufeh/TestPainless/features/DivideAndConquer/MainPainless.h"
int DivideAndConquer_run(int argc, char **argv){
		int res = working_run(argc,argv);
			if (res!=0)
				return res;
			//CubeAndConquer *m =new CubeAndConquer(3);
			printf("CubeAndConquer\n");
			return 0;
	}

//**** Layer SequentialeEngine ****
private:
#line 2 "/home/shokoufeh/TestPainless/features/SequentialeEngine/MainPainless.h"

public:
	inline 
#line 4 "/home/shokoufeh/TestPainless/features/SequentialeEngine/MainPainless.h"
int SequentialeEngine_run(int argc, char **argv){
		int res = DivideAndConquer_run(argc,argv);
			if (res!=0)
				return res;
			//Maple *m =new Maple(3);
			printf("SEQ\n");
			return 0;
	}

//**** Layer CDCL ****
private:
#line 2 "/home/shokoufeh/TestPainless/features/CDCL/MainPainless.h"

public:
	inline 
#line 4 "/home/shokoufeh/TestPainless/features/CDCL/MainPainless.h"
int CDCL_run(int argc, char **argv) {
			int res = SequentialeEngine_run(argc,argv);
			if (res!=0)
				return res;

			printf(" CDCL");
			return 0;
		}


//**** Layer Maple ****
private:
#line 2 "/home/shokoufeh/TestPainless/features/Maple/MainPainless.h"

public:
	int run(int argc, char **argv){
		int res = CDCL_run(argc,argv);
			if (res!=0)
				return res;
			//Maple *m =new Maple(3);
			printf("Maple \n");
			return 0;
	}

#line 29 "/home/shokoufeh/TestPainless/features/Painless/MainPainless.h"
};
#endif //MainPainless_h__included

